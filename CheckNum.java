//  5/7ソフトウェア開発実験 実習基礎データ

public class CheckNum {
    public static void main(String[] args) {
        Integer num;

        num = getNum(); // プロンプトを出して数字を入力させる
        System.out.println("偶数ですか？ >" + conv(isEven(num)));
        System.out.println("素数ですか？ >" + conv(isPrime(num)));
    }

    // 引数を「はい」「いいえ」に変換して返す
    public static String conv(Boolean f) {
        if(f){
		return "はい";
	}else{
		return "いいえ";
	}
    }

    // 適当なプロンプトを出力し、数値を入力させる(その数値を返す)
    public static Integer getNum() {
      System.out.print("数値を入力してください：");
      Scanner scan = new Scanner(System.in);
      int num = scan.nextInt();
      int i = Integer.valueOf(num);
      return 0;
    }

    // 渡された値が偶数かをチェックする(戻り値はBoolean)
    public static Boolean isEven(Integer n) {
        if(n%2==0){
		return true;
	}else{
		return false;
	}
    }

    // 渡された値が素数かをチェックする(戻り値はBoolean)
    public static Boolean isPrime(Integer n) {

        boolean sosu = true;
   	for(int i=2; i<n; i++){
    		if(n%i==0)
        		sosu=false;
  			 break;
       		 }
	}
	return sosu;

    }
}
